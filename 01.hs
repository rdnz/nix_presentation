{-# language OverloadedStrings #-}

import Data.Map (Map, (!))
import Data.Map qualified as M
import Data.Text (Text)
import Data.ByteString (ByteString)

fetchTarball = undefined
importDirectory = undefined
mkDerivation = undefined
interpret = undefined
interpret :: ByteString -> a

-- { interface
type Binary = ByteString -- list of bits

mkDerivation ::
  -- | dependencies
  [Binary] ->
  -- | source tarball
  ByteString ->
  -- | build commands
  Text ->
  -- | built binary
  Binary

fetchTarball :: Text -> ByteString
importDirectory :: FilePath -> ByteString
-- } interface

myProgram00 =
  let
    ghc :: Binary
    ghc =
      mkDerivation
        [perl, autoconf, automake]
        (fetchTarball "https://downloads.haskell.org/ghc-9.4.3-src.tar")
        "..."
    cabal :: Binary
    cabal = mkDerivation...
    zlib :: Binary
    zlib = mkDerivation...
    perl :: Binary
    perl = mkDerivation...
    autoconf :: Binary
    autoconf = mkDerivation...
    automake :: Binary
    automake = mkDerivation...
  in
-- { 00


















    mkDerivation
      [ghc, cabal, zlib]
      (importDirectory ".")
      "cabal build && cp $(cabal list-bin exes) $out"
  :: Binary
-- } 00

myProgram01 =
-- { 01
  let
    ghc :: Binary
    ghc =
      mkDerivation
        [perl, autoconf, automake]
        (fetchTarball "https://downloads.haskell.org/ghc-9.4.3-src.tar")
        "..."
    cabal :: Binary
    cabal = mkDerivation...
    zlib :: Binary
    zlib = mkDerivation...
    perl :: Binary
    perl = mkDerivation...
    autoconf :: Binary
    autoconf = mkDerivation...
    automake :: Binary
    automake = mkDerivation...
  in
    mkDerivation
      [ghc, cabal, zlib]
      (importDirectory ".")
      "cabal build && cp $(cabal list-bin exes) $out"
  :: Binary
-- } 01

myProgram1 =
-- { 1
  let
    pkgs :: Map Text Binary
    pkgs = M.fromList
      [
        (
          "ghc",
          mkDerivation
            [pkgs ! "perl", pkgs ! "autoconf", pkgs ! "automake"]
            (fetchTarball "https://downloads.haskell.org/ghc-9.4.3-src.tar")
            "..."
        ),
        ("cabal", mkDerivation...),
        ("zlib", mkDerivation...),
        ("perl", mkDerivation...),
        ("autoconf", mkDerivation...),
        ("automake", mkDerivation...)
      ]
  in
    mkDerivation
      [pkgs ! "ghc", pkgs ! "cabal", pkgs ! "zlib"]
      (importDirectory ".")
      "cabal build && cp $(cabal list-bin exes) $out"
  :: Binary
-- } 1

myProgram200 =
-- { 200
  let
    pkgs :: Map Text Binary
    pkgs = M.fromList
      [
        (
          "ghc",
          mkDerivation
            [pkgs ! "perl", pkgs ! "autoconf", pkgs ! "automake"]
            (fetchTarball "https://downloads.haskell.org/ghc-9.4.3-src.tar")
            "..."
        ),
        ("cabal", mkDerivation...),
        ("zlib", mkDerivation...),
        ("perl", mkDerivation...),
        ("autoconf", mkDerivation...),
        ("automake", mkDerivation...)
      ]
  in pkgs
  :: Map Text Binary
-- } 200

myProgram201 =
-- { 201
  "let\
  \  pkgs :: Map Text Binary\
  \  pkgs = M.fromList\
  \    [\
  \      (\
  \        \"ghc\",\
  \        mkDerivation\
  \          [pkgs ! \"perl\", pkgs ! \"autoconf\", pkgs ! \"automake\"]\
  \          (fetchTarball \"https://downloads.haskell.org/ghc-9.4.3-src.tar\")\
  \          \"\"\
  \      ),\
  \      (\"cabal\", mkDerivation [] \"\" \"\"),\
  \      (\"zlib\", mkDerivation [] \"\" \"\"),\
  \      (\"perl\", mkDerivation [] \"\" \"\"),\
  \      (\"autoconf\", mkDerivation [] \"\" \"\"),\
  \      (\"automake\", mkDerivation [] \"\" \"\")\
  \    ]\
  \in pkgs\
  \:: Map Text Binary\
  \"
-- } 201

myProgram21 =
-- { 21
  let
    pkgs :: Map Text Binary
    pkgs =
      interpret
        (fetchTarball
          "https://github.com/NixOS/nixpkgs/archive/7edcdf7b169c33c.tar.gz"
        )
  in
    mkDerivation
      [pkgs ! "ghc", pkgs ! "cabal", pkgs ! "zlib"]
      (importDirectory ".")
      "cabal build && cp $(cabal list-bin exes) $out"
  :: Binary
-- } 21

{-
myProgram3 =
-- { 30
  let
    pkgs :: Map Text Binary
    pkgs =
      interpret
        (fetchTarball
          "https://github.com/NixOS/nixpkgs/archive/7edcdf7b169c33c.tar.gz"
          "05rpnsnkwibj36vcmxd55ms2brl3clbi5gh5cnks6qaw2x6mdsag"
        )
  in
    mkDerivation
      [pkgs ! "ghc", pkgs ! "cabal", pkgs ! "zlib"]
      (importDirectory ".")
      "cabal build && cp $(cabal list-bin exes) $out"
  :: Binary
-- } 30
-}

{-
myProgram4 =
-- { 31
  let
    pkgs :: Map Text Binary
    pkgs = M.fromList
      [
        (
          "ghc",
          mkDerivation
            [pkgs ! "perl", pkgs ! "autoconf", pkgs ! "automake"]
            (
              fetchTarball
                "https://downloads.haskell.org/ghc-9.4.3-src.tar"
                "eaf63949536ede50ee39179f2299d5094eb9152d87cc6fb2175006bc98e8905a"
            )
            "..."
        ),
        ("cabal", mkDerivation...),
        ("zlib", mkDerivation...),
        ("perl", mkDerivation...),
        ("autoconf", mkDerivation...),
        ("automake", mkDerivation...)
      ]
  in pkgs
  :: Map Text Binary
-- } 31
-}
